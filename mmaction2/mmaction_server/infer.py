from flask import Flask, request, jsonify
from imageio import imread as ioread
import logging
import base64
import cv2
import os
import numpy as np
import torch
import torchvision.transforms as transforms
from PIL import Image
from mmcv import Config
from mmaction.apis import init_recognizer, inference_recognizer
from mmcv.parallel import collate, scatter
from mmaction.datasets.pipelines import Compose

log = logging.getLogger('werkzeug')

app = Flask(__name__)
mpath = "" #/home/models"
model_file_name = "best_model.pth"
port = 7860

EXCLUED_STEPS = [
    'OpenCVInit', 'OpenCVDecode', 'DecordInit', 'DecordDecode', 'PyAVInit',
    'PyAVDecode', 'RawFrameDecode'
]

# transform = transforms.Compose([transforms.ToPILImage(),
#                                 transforms.Resize([448, 448]),
#                                 transforms.ToTensor()])

cfg = Config.fromfile('configs/recognition/tsn/tsn_r50_video_1x1x8_100e_kinetics400_rgb.py')
#cfg = Config.fromfile('configs/recognition/tsn/tsn_r50_inference_1x1x3_100e_kinetics400_rgb.py')

# Modify num classes of the model in cls_head
cfg.model.cls_head.num_classes = 2
cfg.model.test_cfg.average_clips = 'prob'

device = "cuda:0"
device = torch.device(device)

print("[INFO] Loading Inference Model...")
model = init_recognizer(cfg, os.path.join(mpath, model_file_name), device=device)

classes = ["Non-Violence", "Violence"]

data = dict(img_shape=None, modality='RGB', label=-1)

pipeline = cfg.data.test.pipeline
pipeline_ = pipeline.copy()
for step in pipeline:
    if 'SampleFrames' in step['type']:
        sample_length = step['clip_len'] * step['num_clips']
        data['num_clips'] = step['num_clips']
        data['clip_len'] = step['clip_len']
        pipeline_.remove(step)
    if step['type'] in EXCLUED_STEPS:
        pipeline_.remove(step)
test_pipeline = Compose(pipeline_)

def predict(tracks):

    if data["img_shape"] is None:
        data["img_shape"] = (448, 448)

    cur_data = data.copy()
    cur_data['imgs'] = list(np.array(tracks))
    cur_data = test_pipeline(cur_data)
    cur_data = collate([cur_data], samples_per_gpu=1)
    if next(model.parameters()).is_cuda:
        cur_data = scatter(cur_data, [device])[0]

    with torch.no_grad():
        y_pred = model(return_loss=False, **cur_data)[0]
        #print(y_pred)
        #print(type(y_pred))
        #y_pred = torch.nn.functional.softmax(torch.as_tensor(y_pred))
        #y_pred = y_pred.detach().cpu().numpy().squeeze()
        return y_pred


@app.route("/infer_post/", methods=["POST"])
def infer():

    content = request.get_json()
    tracks = []
    tracks_b64 = content["track"]

    for img_b64 in tracks_b64:
        img = ioread(base64.b64decode(str(img_b64.split("base64,")[1].replace(" ", "+"))))
        tracks.append(img)

    y_pred = predict(tracks)

    _id = y_pred.argmax()
    #class_pred = classes[_id]
    confidence = y_pred[_id]

    confidence = "{0:.2f}".format(confidence)

    if _id == 1 and float(confidence) >= 0.7:
        to_send = {"success": True, "conf": confidence, "label": "Violence"}
        return jsonify(to_send)

    to_send = {"success": False, "conf": "1.0", "label": "NonViolence"}
    return jsonify(to_send)
