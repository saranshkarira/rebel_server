### The model is trained using MMAction2 framework by Devbrat. The training script resides with him. The dataset format is given in two text files.

- https://github.com/open-mmlab/mmaction2
- https://mmaction2.readthedocs.io/en/latest/

Dataset format

```
/custom_dataset
        /train
                video_1.mp4
                video_2.mp4
                .

        /val
                video_100.mp4
                video_101.mp4
                .

        /train.txt
        /val.txt

```

- train.txt and val.txt contain video_file_name and a integer representing the label.
- 0: nonViolence and 1: violence


### Command to run server:
```
docker run --name temp --rm -v $HOME/rebel/mmaction2/mmaction_server/:/home --runtime=nvidia --net=host -ti 192.168.0.100:5000/mmaction:v1 
```

