# Dependencies
import os
import cv2
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import load_model
import time

def resize_with_padding(frame, size=448):

    old_size = frame.shape[:2]  # old_size is in (height, width) format

    ratio = float(size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    # new_size should be in (width, height) format

    frame = cv2.resize(frame, (new_size[1], new_size[0]))

    delta_w = size - new_size[1]
    delta_h = size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    # color = [0, 0, 0]
    new_frame = cv2.copyMakeBorder(frame, top, bottom, left, right, cv2.BORDER_CONSTANT)

    return new_frame


# loading model
#model_path = "new_train/new_best_model.h5"
model_path = "weights/best_model.h5"

rebel_model = load_model(model_path)

# video source for prediction
test_dir = "../dataset/test"
video_clip = "new_Violence.mp4"
output_video_clip = "18_aug_result_new_" + video_clip
#output_video_clip = "18_aug_result_" + video_clip

# initializing cv2 to read video clips
cap = cv2.VideoCapture(os.path.join(test_dir, video_clip))

# by default VideoCapture returns float instead of int
width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
fps = int(cap.get(cv2.CAP_PROP_FPS))
codec = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter(os.path.join(test_dir, output_video_clip), codec, fps, (width, height))

_classes = ['nonViolence', 'violence']

start_time = time.time()

while(cap.isOpened()):

    ret, frame = cap.read()

    if ret:
        
        # resize frame
        new_frame = frame.copy()
        rsz_frame = resize_with_padding(new_frame)
        rsz_frame = rsz_frame * 1.0/255

        # convert to tensor
        frame_tensor = np.expand_dims(rsz_frame, axis=0)

        # predict
        prediction = rebel_model.predict(frame_tensor)
        end_time = time.time()
        print(end_time - start_time)
        start_time = end_time

        _id = prediction[0].argmax()
        print(prediction[0])
        class_pred = _classes[_id]
        confidence = prediction[0][_id]

        print(class_pred, confidence)

        # post-process
        if _id == 1:
            cv2.putText(frame, class_pred+"--"+ str(confidence) , (75, 75), 0, 1, (0, 0, 255), 1)
        
        out.write(frame)

    else:
        break


cap.release()
cv2.destroyAllWindows()
