import os.path as osp
import os
import cv2
import numpy as np
import random

import tensorflow as tf
from tensorflow.keras.models import Model, load_model, Sequential
from tensorflow.keras.layers import GlobalAveragePooling2D, Dense, Input, Dropout
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import ReLU
from tensorflow.keras.layers import Flatten
from tensorflow.keras.regularizers import l2
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau, LearningRateScheduler, TensorBoard

# Utility class that constitues a lot of helper method used across this assignment
class Utils:
    def __init__(self, dataset_dir):
        self.dataset_dir = dataset_dir


    def load_data(self, return_label=True):

        print('[INFO] Reading dataset...')

        files = []
        labels = []

        for (rootDir, dirNames, filenames) in os.walk(self.dataset_dir):
            # loop over the filenames in the current directory
            for filename in filenames:
                imagePath = osp.join(rootDir.split(osp.sep)[-1], filename)
                files.append(imagePath)
                if return_label:
                    label = osp.join(rootDir, filename).split(osp.sep)[-2]
                    labels.append(label)

        return files, labels


    # method that returns data generator
    def get_df_gen(self, datagen, dataframe, batch_size, y_col = "label", target_size = (448, 448), shuffle = True):

        if y_col:
            class_mode = "categorical"
        else:
            class_mode = None

        return datagen.flow_from_dataframe(
            dataframe,
            directory = self.dataset_dir,
            x_col = "image_path",
            y_col = y_col,
            class_mode = class_mode,
            batch_size = batch_size,
            shuffle = shuffle,
            seed = 42,
            interpolation = "nearest"
        )

    
    # method that return classification model
    def get_model(self, base_model, classes):
        x = base_model.output
        x = GlobalAveragePooling2D()(x)
        x = Dense(500, activation = 'relu')(x)
        x = Dropout(0.3)(x)
        
        predictions = Dense(classes, kernel_regularizer = l2(0.005), activation = 'sigmoid', name = 'output')(x)

        model = Model(inputs = base_model.input, outputs = predictions, name = "model")

        return model

    
    # method that sets-up callbacks
    def set_callbacks(self, weights_dir, best_model_path, metrics):

        # To store weights on minimum validation loss values
        checkpoint = ModelCheckpoint(os.path.join(weights_dir, best_model_path), monitor=metrics, 
                                    verbose=1, save_best_only = True, mode='min', period=1)

        # to reduce learning rate on plateau
        reduce_lr = ReduceLROnPlateau(monitor=metrics, patience=3, verbose=1, min_lr=0.000001)

        # lr_scheduler = LearningRateScheduler(scheduler)

        # early stopping the training when calidation loss does not decrease after 
        early_stop = EarlyStopping(monitor=metrics, patience=6, verbose=1)

        tb = TensorBoard(log_dir='new_logs')

        # list of callbacks
        callbacks = [checkpoint, early_stop, reduce_lr, tb]

        return callbacks

