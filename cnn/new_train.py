import os
import cv2
import pandas as pd
import numpy as np
from new_utils import Utils
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications.resnet50 import ResNet50, preprocess_input
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras import backend as K
from sklearn.metrics import classification_report, confusion_matrix

K.clear_session()
#os.environ["CUDA_VISIBLE_DEVICES"] = "0"

dataset_dir = "../../dataset/final_aspect"
test_size = 0.2

# instantiating utility class
utils = Utils(dataset_dir)

images, labels = utils.load_data()

_classes = np.unique(labels)
num_classes = len(_classes)
assert(len(images) == len(labels))

print("[INFO] Found {} Images and Labels belonging to {} classes = {}".format(len(images), num_classes, _classes))

# splitting dataset into training and validation and test set
trainX, validationX, trainY, validationY = train_test_split(images, labels, test_size = test_size, shuffle=True, stratify = labels)

trainX, testX, trainY, testY = train_test_split(trainX, trainY, test_size = 0.10, shuffle=True, stratify = trainY)

"""
trainX = trainX[:500]
trainY = trainY[:500]
validationX = validationX[:100]
validationY = validationY[:100]
testX = testX[:50]
testY = testY[:50]
"""

train_df = pd.DataFrame({'image_path': trainX, 'label': trainY})
valid_df = pd.DataFrame({'image_path': validationX, 'label': validationY})
test_df = pd.DataFrame({'image_path': testX, 'label': testY})

print("[INFO] Training images count.. ", train_df.shape[0])
print("[INFO] Validation images count.. ", valid_df.shape[0])
print("[INFO] Testing images count.. ", test_df.shape[0])

batch_size = 24

train_datagen = ImageDataGenerator(rotation_range = 10,
                                   zoom_range = 0.1,
                                   width_shift_range=0.1,
                                   height_shift_range=0.1,
                                   brightness_range = [0.5, 0.75],
                                   rescale = 1./255)

valid_datagen = ImageDataGenerator(rescale = 1./255)
test_datagen = ImageDataGenerator(rescale = 1./255)

train_generator = utils.get_df_gen(train_datagen, train_df, batch_size)
valid_generator = utils.get_df_gen(valid_datagen, valid_df, batch_size)
test_generator = utils.get_df_gen(test_datagen, test_df, batch_size, shuffle=False)

input_shape = (448, 448, 3)

# mobilenet base model
# base_model = MobileNetV2(weights = "imagenet", include_top = False, input_shape = input_shape)

# resnet base model
base_model = ResNet50(weights = "imagenet", include_top = False, input_shape = input_shape)

model = utils.get_model(base_model, num_classes)
print("\n[INFO] Model summary...")
model.summary()


epochs = 30
learning_rate = 1e-3
weights_path = 'new_weights'
if not os.path.exists(weights_path):
    os.makedirs(weights_path)

STEP_SIZE_TRAIN = train_generator.n//train_generator.batch_size
STEP_SIZE_VALID = valid_generator.n//valid_generator.batch_size

callbacks = utils.set_callbacks(weights_path, 'new_best_model.h5', 'val_loss')

print("\n[INFO] Compiling model...")
sgd = SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True)
adam = Adam(lr = learning_rate, decay = learning_rate / epochs)
model.compile(optimizer = sgd, loss = 'binary_crossentropy', metrics=['accuracy'])

print("\n[INFO] Training model...")
history = model.fit(train_generator,
        steps_per_epoch = STEP_SIZE_TRAIN,
        validation_data = valid_generator,
        validation_steps = STEP_SIZE_VALID,
        epochs = epochs,
        verbose = 1,
        callbacks = callbacks)


print("\n[INFO] Loading trained model...\n")
model = load_model("new_weights/new_best_model.h5")

predict = model.evaluate_generator(test_generator)
test_loss, test_accuracy = predict
print("\n[INFO] Test Data: Loss = {}, Accuracy = {}\n".format(test_loss, test_accuracy))

y_pred = model.predict(test_generator)
y_pred = np.argmax(y_pred, axis=1)

print(classification_report(test_generator.classes, y_pred))

cm = confusion_matrix(test_generator.classes, y_pred)
print(cm)


train_df.to_csv('train.csv')
valid_df.to_csv('valid.csv')
test_df.to_csv('test.csv')
