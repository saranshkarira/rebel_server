
from __future__ import print_function
import numpy as np
import cv2
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import load_model
import time
  
from flask import Flask, request, jsonify
from imageio import imread as ioread
import logging
import base64
from base64 import b64decode
import json


#vars shift them in config file later

log = logging.getLogger('werkzeug')
#log.disabled = True
#app.logger.disabled = True

app = Flask(__name__)
cuda = 1
mpath = "best_model.h5"
port = 7860

# loading trained model
model = load_model(mpath)

classes = ["nonViolence", "violence"]


def resize_with_padding(frame, size=448):

    old_size = frame.shape[:2]  # old_size is in (height, width) format

    ratio = float(size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    # new_size should be in (width, height) format

    frame = cv2.resize(frame, (new_size[1], new_size[0]))

    delta_w = size - new_size[1]
    delta_h = size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    new_frame = cv2.copyMakeBorder(frame, top, bottom, left, right, cv2.BORDER_CONSTANT)

    return new_frame


@app.route("/infer/")
def infer():
    c_out = {}
    img_loc = request.args.get("url")
    if img_loc is None:
        b64_url = request.args.get('base64')
        #Decode into normal PIL or OPENCV
        #print(b64_url)
        img = ioread(base64.b64decode(str(b64_url.split('base64,')[1].replace(' ','+'))))
        #img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    else:
        img = cv2.imread(img_loc)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    
    # resize img
    rsz_img = resize_with_padding(img.copy())
    rsz_img = rsz_img * 1.0/255

    # convert to tensor
    img_tensor = np.expand_dims(rsz_img, axis=0)

    # predict
    prediction = model.predict(img_tensor)
    _id = prediction[0].argmax()
    class_pred = classes[_id]
    confidence = prediction[0][_id]

    # sending only violence events
    if confidence > 0.75 and _id == 1:
        to_send = {"success": True, "conf": [confidence], "labels": [class_pred]}
    else:
        to_send = {"success": False}
    
    return jsonify(to_send)
