import os
import glob
import numpy as np
import torch
import torchvision.transforms as transforms
import torch.utils.data as data
import matplotlib.pyplot as plt
from functions import *
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
from sklearn.metrics import accuracy_score
import pandas as pd
import pickle
import cv2
from PIL import Image

data_path = "new_test/"
save_model_path = "./test_weights/"

# use same encoder CNN saved!
CNN_fc_hidden1, CNN_fc_hidden2 = 1024, 768
CNN_embed_dim = 512   # latent dim extracted by 2D CNN
res_size = 448        # ResNet image size
dropout_p = 0.0       # dropout probability

# use same decoder RNN saved!
RNN_hidden_layers = 3
RNN_hidden_nodes = 512
RNN_FC_dim = 256

# training parameters
k = 2             # number of target category
batch_size = 48

# data loading parameters
use_cuda = torch.cuda.is_available()                   # check if GPU exists
device = torch.device("cuda" if use_cuda else "cpu")   # use CPU or GPU

transform = transforms.Compose([transforms.ToPILImage(),
                                transforms.Resize([res_size, res_size]),
                                transforms.ToTensor(),
                                transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])

# reload CRNN model
cnn_encoder = ResCNNEncoder(fc_hidden1=CNN_fc_hidden1, fc_hidden2=CNN_fc_hidden2, drop_p=dropout_p, CNN_embed_dim=CNN_embed_dim).to(device)
rnn_decoder = DecoderRNN(CNN_embed_dim=CNN_embed_dim, h_RNN_layers=RNN_hidden_layers, h_RNN=RNN_hidden_nodes, 
                         h_FC_dim=RNN_FC_dim, drop_p=dropout_p, num_classes=k).to(device)

cnn_encoder.load_state_dict(torch.load(os.path.join(save_model_path, 'cnn_encoder_epoch4.pth')))
rnn_decoder.load_state_dict(torch.load(os.path.join(save_model_path, 'rnn_decoder_epoch4.pth')))
print('CRNN model reloaded!')
cnn_encoder.eval()
rnn_decoder.eval()

def infer(X):
    
    X = [transform(x) for x in X]
    X = torch.stack(X, dim=0)
    X = torch.unsqueeze(X, 0)

    X = X.to(device)
    output = rnn_decoder(cnn_encoder(X))
    output = torch.nn.functional.softmax(output)
    y_pred = output.detach().cpu().numpy().squeeze()
    print(y_pred)
    return y_pred


preds = {}
print(glob.glob(os.path.join(data_path, "bangalore*.mp4")))
for vid in glob.glob(os.path.join(data_path, "bangalore*.mp4")):

    preds[vid] = []

    cap = cv2.VideoCapture(vid)
    print("predicting on ", vid)
    counter = 1
    X = []

    while(True):
        ret, frame = cap.read()

        if ret == False:
            X.append(previous_frame)
            counter += 1

            if counter == 25:
                preds[vid].append(infer(X))
                break
            
        else:
            #cv2.imwrite("temp.jpg", frame)
            #frame = Image.open("temp.jpg")
            #frame = frame[0:875, 0:1150]
            X.append(frame)
            previous_frame = frame

            counter += 1

            if counter == 25:
                preds[vid].append(infer(X))
                X = []
                counter = 1


classes = ["nonViolence", "violence"]

for vid in list(preds.keys()):

    output_video_clip = "crnn_pred_" + vid.split(os.path.sep)[-1]
    #output_video_clip = output_video_clip.replace(".dav", ".mp4")
    
    cap = cv2.VideoCapture(vid)
    # by default VideoCapture returns float instead of int
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    codec = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(output_video_clip, codec, fps, (width, height))

    counter = 0
    index = 0

    while(cap.isOpened()):

        ret, frame = cap.read()
        counter += 1

        if ret == False:
            break
        else:

            if counter == 25:
                index += 1
                counter = 0

            pred = preds[vid][index]

            _id = pred.argmax()
            class_pred = classes[_id]
            confidence = pred[_id]

            print("{} = {0:.2f} ".format(class_pred, confidence))
            #if _id == 1 and confidence >= 0.90:
            #cv2.rectangle(frame, (0,0), (875, 1150), (0,255,0), 2)
            cv2.putText(frame, "{} = {0:.2f} ".format(class_pred, confidence) , (150, 150), 0, 2, (0, 0, 255), 2)

            out.write(frame)

    cap.release()
    cv2.destroyAllWindows()



