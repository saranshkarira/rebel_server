docker stop rebel_v1
docker start rebel_v1
docker exec -it rebel_v1 bash -c " cd /workspace ; \
export FLASK_APP=crnn_infer.py;\
export LC_ALL=C.UTF-8;\
export LANG=C.UTF-8;\
flask run --host=0.0.0.0 --port=7860;"
