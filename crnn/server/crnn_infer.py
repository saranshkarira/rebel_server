from flask import Flask, request, jsonify
from imageio import imread as ioread
import logging
import base64
from base64 import b64decode
import json
import cv2
import os
import glob
import numpy as np
import torch
import torchvision.transforms as transforms
import torch.utils.data as data
import matplotlib.pyplot as plt
from torch.autograd import Variable
from functions import *

#vars shift them in config file later

log = logging.getLogger('werkzeug')
#log.disabled = True
#app.logger.disabled = True

app = Flask(__name__)
cuda = 1
mpath = "/home/crnn_models"
port = 7860

cnn_model_path = os.path.join(mpath, "cnn_encoder.pth")
rnn_model_path = os.path.join(mpath, "rnn_decoder.pth")

# use same encoder CNN saved!
CNN_fc_hidden1, CNN_fc_hidden2 = 1024, 768
CNN_embed_dim = 512   # latent dim extracted by 2D CNN
res_size = 448        # ResNet image size
dropout_p = 0.0       # dropout probability

# use same decoder RNN saved!
RNN_hidden_layers = 3
RNN_hidden_nodes = 512
RNN_FC_dim = 256

k = 2   # number of target category

# data loading parameters
use_cuda = torch.cuda.is_available()                   # check if GPU exists
device = torch.device("cuda" if use_cuda else "cpu")   # use CPU or GPU

transform = transforms.Compose([transforms.ToPILImage(),
                                transforms.Resize([res_size, res_size]),
                                transforms.ToTensor(),
                                transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])

# reload CRNN model
cnn_encoder = ResCNNEncoder(fc_hidden1=CNN_fc_hidden1, fc_hidden2=CNN_fc_hidden2, drop_p=dropout_p, CNN_embed_dim=CNN_embed_dim).to(device)
rnn_decoder = DecoderRNN(CNN_embed_dim=CNN_embed_dim, h_RNN_layers=RNN_hidden_layers, h_RNN=RNN_hidden_nodes, 
                         h_FC_dim=RNN_FC_dim, drop_p=dropout_p, num_classes=k).to(device)

cnn_encoder.load_state_dict(torch.load(cnn_model_path))
rnn_decoder.load_state_dict(torch.load(rnn_model_path))
print('CRNN model loaded!')
cnn_encoder.eval()
rnn_decoder.eval()

classes = ["nonViolence", "violence"]

print(classes)

def predict(X):
    
    # X is list of frames(Cv2 image)

    X = [transform(x) for x in X]
    X = torch.stack(X, dim=0)
    X = torch.unsqueeze(X, 0)

    with torch.no_grad():
        X = X.to(device)
        output = rnn_decoder(cnn_encoder(X))
        output = torch.nn.functional.softmax(output)
        y_pred = output.detach().cpu().numpy().squeeze()
        print(y_pred)

        # [0.99065685 0.0093431] (numpy array)
        """
        _id = y_pred.argmax()
        class_pred = classes[_id]
        confidence = y_pred[_id]
        """

        return y_pred


@app.route("/infer_post/", methods=["POST"])
def infer():

    content = request.get_json()
    tracks = []
    tracks_b64 = content["track"]

    for img_b64 in tracks_b64:
        img = ioread(base64.b64decode(str(img_b64.split("base64,")[1].replace(" ", "+"))))
        tracks.append(img)

    
    y_pred = predict(tracks)

    _id = y_pred.argmax()
    class_pred = classes[_id]
    confidence = y_pred[_id]

    confidence = "{0:.2f}".format(confidence)

    if _id == 1 and float(confidence) >= 0.75:
        to_send = {"success": True, "conf": confidence, "label": "Violence"}
        return jsonify(to_send)

    to_send = {"success": False, "conf": "1.0", "label": "NonViolence"}
    return jsonify(to_send)
