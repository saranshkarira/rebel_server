# README #

This repo contains scripts and instructions for the rebel detection work. 4 different models/methods were explored:
- CNN (Tensorflow)
- CRNN (Pytorch, ResnetCRNN)
- MMAction2 (Pytorch)
- Gluoncv (Pytorch)

All the server codes, docker files, train and infer scripts are available in respective folders.
