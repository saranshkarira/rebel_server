### This codebase contains custom training scripts for actio-recognition in gluoncv(pytorch). 

- docker image: 192.168.0.100:5000/gluoncv:v0.0.1
- https://cv.gluon.ai/contents.html
- https://medium.com/apache-mxnet/which-one-is-the-best-algorithm-for-video-action-recognition-298fb5c4ad4f


### Clone this repo: https://github.com/dmlc/gluon-cv and copy the fine_tune scripts provided in this repo at path - "https://github.com/dmlc/gluon-cv/tree/master/scripts/action-recognition/" and run following command:

```
CUDA_VISIBLE_DEVICES=0 python finetune_torch_action_recog.py --config-file custom_configs/tpn_resnet50_f32s2_custom.yaml
```

### The config files are updated with the dataset path and all other required hyper-parameters. The format for dataset remains same as it is in MMAction2 i.e. following structure:

```
/custom_dataset
	/train
		video_1.mp4
		video_2.mp4
		.

	/val
		video_100.mp4
                video_101.mp4
                .
		
	/train.txt
	/val.txt

```
- train.txt and val.txt contain video_file_name and a integer representing the label.
- 0: nonViolence and 1: violence
