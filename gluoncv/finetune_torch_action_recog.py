# Dependencies
import os, logging
import numpy as np
import time
import torch
import argparse
import torch.nn as nn
import torch.optim
from tensorboardX import SummaryWriter

from gluoncv.torch.data import build_dataloader
from gluoncv.torch.utils.model_utils import save_model
#from gluoncv.torch.utils.task_utils import validation_classification
from gluoncv.torch.engine.config import get_cfg_defaults

from gluoncv.torch.utils.utils import build_log_dir
from gluoncv.torch.utils.lr_policy import GradualWarmupScheduler
from gluoncv.torch.model_zoo.action_recognition.slowfast import slowfast_4x16_resnet50_kinetics400
from gluoncv.torch.model_zoo.action_recognition.tpn import tpn_resnet50_f32s2_custom
from gluoncv.torch.model_zoo import get_model

from gluoncv.torch.utils.utils import AverageMeter

logger = logging.getLogger()

def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].reshape(-1).float().sum(0, keepdim=True)
        res.append(correct_k.mul_(100.0 / batch_size))

    return res

def train_classification(base_iter,
                         model,
                         dataloader,
                         epoch,
                         criterion,
                         optimizer,
                         cfg,
                         writer=None):
    """Task of training video classification"""
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    model.train()
    end = time.time()
    for step, data in enumerate(dataloader):
        base_iter = base_iter + 1

        train_batch = data[0].cuda()
        train_label = data[1].cuda()
        data_time.update(time.time() - end)

        outputs = model(train_batch)
        loss = criterion(outputs, train_label)
        if cfg.CONFIG.DATA.NUM_CLASSES < 5:
            prec1, prec5 = accuracy(outputs.data, train_label, topk=(1, 1))
            # Tricky solution for datasets with less than 5 classes, top5 acc is always set to 100%
            #prec5 = [100]
        else:
            prec1, prec5 = accuracy(outputs.data, train_label, topk=(1, 5))

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        losses.update(loss.item(), train_label.size(0))
        top1.update(prec1.item(), train_label.size(0))
        top5.update(prec5.item(), train_label.size(0))

        batch_time.update(time.time() - end)
        end = time.time()
    
        if step % cfg.CONFIG.LOG.DISPLAY_FREQ_TRAIN == 0 and cfg.DDP_CONFIG.GPU_WORLD_RANK == 0:
            temp_str = ""
            logger.info('-------------------------------------------------------')
            for param in optimizer.param_groups:
                lr = param['lr']
            print_string = 'lr: {lr:.5f}'.format(lr=lr)
            logger.info(print_string)
            print_string = 'Epoch: [{0}][{1}/{2}]'.format(
                epoch, step + 1, len(dataloader))
            
            temp_str += "\n" + print_string + " "

            logger.info(print_string)
            print_string = 'data_time: {data_time:.3f}, batch time: {batch_time:.3f}'.format(
                data_time=data_time.val, batch_time=batch_time.val)
            logger.info(print_string)
            print_string = 'loss: {loss:.5f}'.format(loss=losses.avg)
            temp_str += print_string + " "

            logger.info(print_string)
            print_string = 'Top-1 accuracy: {top1_acc:.2f}%, Top-5 accuracy: {top5_acc:.2f}%'.format(
                top1_acc=top1.avg, top5_acc=top5.avg)

            temp_str += print_string + " "

            logger.info(print_string)
            iteration = base_iter
            writer.add_scalar('train_loss_iteration', losses.avg, iteration)
            writer.add_scalar('train_top1_acc_iteration', top1.avg, iteration)
            writer.add_scalar('train_top5_acc_iteration', top5.avg, iteration)
            writer.add_scalar('train_batch_size_iteration',
                              train_label.size(0), iteration)
            writer.add_scalar('learning_rate', lr, iteration)

            
            train_path = cfg.CONFIG.LOG.TRAIN_DIR
            if not os.path.exists(train_path):
                os.makedirs(train_path)

            with open(
                os.path.join(train_path,
                             "{}.txt".format(cfg.DDP_CONFIG.GPU_WORLD_RANK)),
                'a') as f:
                f.write(temp_str)
                print(temp_str)

    return base_iter


def validation_classification(model, val_dataloader, epoch, criterion, cfg,
                              writer):
    """Task of validating video classification"""
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()
    model.eval()

    end = time.time()
    with torch.no_grad():
        for step, data in enumerate(val_dataloader):
            data_time.update(time.time() - end)
            val_batch = data[0].cuda()
            val_label = data[1].cuda()
            outputs = model(val_batch)

            loss = criterion(outputs, val_label)
            if cfg.CONFIG.DATA.NUM_CLASSES < 5:
                prec1a, prec5a = accuracy(outputs.data, val_label, topk=(1, 1))
                # Tricky solution for datasets with less than 5 classes, top5 acc is always set to 100%
                #prec5a = 100
            else:
                prec1a, prec5a = accuracy(outputs.data, val_label, topk=(1, 5))

            losses.update(loss.item(), val_batch.size(0))
            top1.update(prec1a.item(), val_batch.size(0))
            top5.update(prec5a.item(), val_batch.size(0))
            batch_time.update(time.time() - end)
            end = time.time()

            if step % cfg.CONFIG.LOG.DISPLAY_FREQ_VAL == 0 and cfg.DDP_CONFIG.GPU_WORLD_RANK == 0:

                temp_str = ""

                logger.info('----validation----')
                print_string = 'Epoch: [{0}][{1}/{2}]'.format(
                    epoch, step + 1, len(val_dataloader))

                temp_str += "\n" + print_string + " "
                logger.info(print_string)
                print_string = 'data_time: {data_time:.3f}, batch time: {batch_time:.3f}'.format(
                    data_time=data_time.val, batch_time=batch_time.val)
                logger.info(print_string)
                print_string = 'loss: {loss:.5f}'.format(loss=losses.avg)
                logger.info(print_string)
                
                temp_str += print_string + " "
                print_string = 'Top-1 accuracy: {top1_acc:.2f}%, Top-5 accuracy: {top5_acc:.2f}%'.format(
                    top1_acc=top1.avg, top5_acc=top5.avg)
                logger.info(print_string)
                temp_str += print_string + " "

                print(temp_str)

        eval_path = cfg.CONFIG.LOG.EVAL_DIR
        if not os.path.exists(eval_path):
            os.makedirs(eval_path)

        with open(
                os.path.join(eval_path,
                             "{}.txt".format(cfg.DDP_CONFIG.GPU_WORLD_RANK)),
                'w') as f:
            f.write("{} {} {} {}\n".format(epoch, losses.avg, top1.avg, top5.avg))
        #torch.distributed.barrier()

        loss_lst, top1_lst, top5_lst = [], [], []
        if cfg.DDP_CONFIG.GPU_WORLD_RANK == 0 and writer is not None:
            data = open(os.path.join(
                eval_path,
                "{}.txt".format(0))).readline().strip().split(" ")
            data = [float(x) for x in data]
            loss_lst.append(data[1])
            top1_lst.append(data[2])
            top5_lst.append(data[3])
            print("Global result:")
            print_string = 'loss: {loss:.5f}'.format(loss=np.mean(loss_lst))
            print(print_string)
            print_string = 'Top-1 accuracy: {top1_acc:.2f}%, Top-5 accuracy: {top5_acc:.2f}%'.format(
                top1_acc=np.mean(top1_lst), top5_acc=np.mean(top5_lst))
            print(print_string)
            writer.add_scalar('val_loss_epoch', np.mean(loss_lst), epoch)
            writer.add_scalar('val_top1_acc_epoch', np.mean(top1_lst), epoch)
            writer.add_scalar('val_top5_acc_epoch', np.mean(top5_lst), epoch)


def main_worker(cfg):
    # create tensorboard and logs
    if cfg.DDP_CONFIG.GPU_WORLD_RANK == 0:
        tb_logdir = build_log_dir(cfg)
        writer = SummaryWriter(log_dir=tb_logdir)
    else:
        writer = None
    #cfg.freeze()

    # create model
    #model = slowfast_4x16_resnet50_kinetics400(cfg)
    #model = tpn_resnet50_f32s2_custom(cfg)
    model = get_model(cfg)

    # Handle gradient ops according to the pretraining flag
    requires_grad=False
    if cfg.CONFIG.MODEL.PRETRAINED == False:
        requires_grad=True

    #print(model)
    #print(cfg.CONFIG.MODEL)

    # # Finetune
    for param in model.parameters():
        param.requires_grad = requires_grad
        # Replace the last fully-connected layer
        # Parameters of newly constructed modules have requires_grad=True by default
    model.fc = nn.Linear(cfg.CONFIG.MODEL.FC_NUM, cfg.CONFIG.MODEL.FINETUNE_CLASS)

    # Use cuda
    DEVICE = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model.to(DEVICE)

    #print(model)

    # create dataset and dataloader
    train_loader, val_loader, train_sampler, val_sampler, mg_sampler = build_dataloader(cfg)
    optimizer = torch.optim.SGD(model.parameters(), lr=cfg.CONFIG.TRAIN.LR, momentum=cfg.CONFIG.TRAIN.MOMENTUM,
                                weight_decay=cfg.CONFIG.TRAIN.W_DECAY)


    # Optimize the learning rate
    if cfg.CONFIG.TRAIN.LR_POLICY == 'Step':
        scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer,
                                                         milestones=cfg.CONFIG.TRAIN.LR_MILESTONE,
                                                         gamma=cfg.CONFIG.TRAIN.STEP)
    elif cfg.CONFIG.TRAIN.LR_POLICY == 'Cosine':
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer,
                                                               T_max=cfg.CONFIG.TRAIN.EPOCH_NUM - cfg.CONFIG.TRAIN.WARMUP_EPOCHS,
                                                               eta_min=0,
                                                               last_epoch=cfg.CONFIG.TRAIN.RESUME_EPOCH)
    else:
        print('Learning rate schedule %s is not supported yet. Please use Step or Cosine.')

    # Warm-up
    if cfg.CONFIG.TRAIN.USE_WARMUP:
        scheduler_warmup = GradualWarmupScheduler(optimizer,
                                                  multiplier=(cfg.CONFIG.TRAIN.WARMUP_END_LR / cfg.CONFIG.TRAIN.LR),
                                                  total_epoch=cfg.CONFIG.TRAIN.WARMUP_EPOCHS,
                                                  after_scheduler=scheduler)


    criterion = nn.CrossEntropyLoss().cuda()

    # hack for top-k prediction
    cfg.CONFIG.DATA.NUM_CLASSES = cfg.CONFIG.MODEL.FINETUNE_CLASS

    base_iter = 0
    for epoch in range(cfg.CONFIG.TRAIN.EPOCH_NUM):
        #print("Epoch = ", epoch)
        # Not using DISTRIBUTED but gluon-torch uses, just turn of in your cfg file
        if cfg.DDP_CONFIG.DISTRIBUTED:
            train_sampler.set_epoch(epoch)

        # Train using gluon-torch functions
        base_iter = train_classification(base_iter, model, train_loader, epoch, criterion, optimizer, cfg, writer=writer)
        if cfg.CONFIG.TRAIN.USE_WARMUP:
            scheduler_warmup.step()
        else:
            scheduler.step()

        if cfg.CONFIG.TRAIN.MULTIGRID.USE_LONG_CYCLE:
            if epoch in cfg.CONFIG.TRAIN.MULTIGRID.LONG_CYCLE_EPOCH:
                mg_sampler.step_long_cycle()


        if epoch % cfg.CONFIG.VAL.FREQ == 0 or epoch == cfg.CONFIG.TRAIN.EPOCH_NUM - 1:
            validation_classification(model, val_loader, epoch, criterion, cfg, writer)

        if epoch % cfg.CONFIG.LOG.SAVE_FREQ == 0:
            if cfg.DDP_CONFIG.GPU_WORLD_RANK == 0 or cfg.DDP_CONFIG.DISTRIBUTED == False:
                save_model(model, optimizer, epoch, cfg)

    if writer is not None:
        writer.close()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Train video action recognition models..")
    parser.add_argument("--config-file", type=str, required=True)
    args = parser.parse_args()

    cfg = get_cfg_defaults(name="action_recognition")
    cfg.merge_from_file(args.config_file)
    main_worker(cfg)
